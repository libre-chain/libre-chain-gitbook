---
description: Libre Glossary of Terms
---

# Glossary

**Libre** - comes from the Latin word līber, via the French and Spanish libre; it shares that root with liberty. It denotes "the state of being free", in the sense of "having freedom" or "liberty".

**DPOS** - Delegated Proof-of-Stake is the consensus algorithm that runs Libre validator nodes. LIBRE holders stake for voting power and then vote for validators. The top 21 validators by vote weight are considered "active" and the next 6 are "standby" - both active and standby validators are paid.

**Wrapped or Pegged BTC and USDT** - these assets are tokenized on Libre that are backed 1:1 by assets held in the decentralized Libre Crosslink MPC bridge. This allows these assets to have new capabilities that are not possible on their native chains - such as near instant finality of transactions, send and receive with no fees, and programmability in smart contracts.&#x20;

There is an easy way that anyone can audit the BTC by looking at the total BTC issued by btc.libre and comparing that to the BTC in all of the addresses in the x.libre accounts table.&#x20;

The [Libre Tools website](https://libre-tools.com) will help you download tables.
