---
description: >-
  Anyone can run a node to validate Libre data and submit API transactions, it's
  as easy as 1,2,3...
---

# Run A Libre Node

1.  Install Libre node software:

    ```
    bash -c "$(curl -sSfL https://gitlab.com/libre-chain/libre-chain-nodes/-/raw/main/install.sh)"
    ```
2.  Update your environment:

    ```
    source ~/.bashrc
    ```
3.  Check logs:

    ```
    tail -f /opt/libre-chain-nodes/libreNode/stderr.txt
    ```

{% hint style="info" %}
_For more advanced configuration and usage - including running a mainnet node - please refer to the full documentation._&#x20;

_Hint: Check_ [_mainnet dir_](https://gitlab.com/libre-chain/libre-chain-nodes/-/tree/main/libreNode/Producer/Mainnet) _for peers and_ [_node.env_](https://gitlab.com/libre-chain/libre-chain-nodes/-/blob/main/libreNode/node.env) _for snapshot link._
{% endhint %}

{% embed url="https://gitlab.com/libre-chain/libre-chain-nodes/-/tree/main" %}
Libre Nodes Git Repo
{% endembed %}

See also [validators.md](validators.md "mention")
