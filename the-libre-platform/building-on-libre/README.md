---
description: >-
  Libre  is the best place to build smart contracts that interact with Bitcoin,
  Stablecoins, and even Bitcoin Lightning.
---

# The Smart Contract Platform

Libre is the first user-focused tool for making stablecoin (USDT) and Bitcoin (BTC) incredibly simple to integrate into existing games, apps, or wallets - or even [build smart contracts](broken-reference) to interact with BTC and USDT.&#x20;

### Integration for Payments

Libre is the simplest way to integrate payments to send and receive Tether and Bitcoin. With one single QR code - anyone in the world can accept a payment of USDT or BTC on Libre chain instantly, with no fees.

### Interacting with Accounts

It's very easy to implement wallet integrations with any of the wallets working on Libre Chain for authentication and signing transactions. Please see the [accounts and wallet](broken-reference) section here for more info.

Developers can use the [open-source Libre-Link npm package](https://socket.dev/npm/package/@libre-chain/libre-link) for authentication and execution of signing requests.
