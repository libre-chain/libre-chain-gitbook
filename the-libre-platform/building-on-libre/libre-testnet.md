---
description: >-
  Libre has an active testnet with a faucet and replications of many apps and
  smart contracts.
---

# Libre Testnet

{% hint style="info" %}
Anyone can use or build on the Libre Testnet. If you need some Testnet Bitcoin or Tether, please join the [Testnet Telegram below](libre-testnet.md#libre-testnet-telegram).
{% endhint %}

### **Libre Testnet Accounts and LIBRE Faucet**

Create Libre testnet accounts and get Libre testnet LIBRE coins.

{% embed url="https://libre-testnet.antelope.tools/faucet" %}
Libre Testnet Faucet and Account Creator
{% endembed %}

### Libre Testnet DeFi and Governance Dashboard

Test out swap, stake, vote, add and remove liquidity, farm, DAO proposals!

{% embed url="https://test-dash.libre.org/" %}
Libre Testnet Dashboard
{% endembed %}

### **Libre Testnet API Endpoints**

API endpoint monitoring and list available here from antelope tools.

{% embed url="https://libre-testnet.antelope.tools/endpoints" %}
Endpoint monitoring.
{% endembed %}

### **Libre Testnet Telegram**

Join the validator / developer chat on Telegram.

{% embed url="https://t.me/+l-Un_5tmHzZkNzhh" %}

### **Libre Testnet Monitors**

The monitors track whether or not validators miss blocks along with other helpful data about the chain.

\- Sweden Simple Monitor [https://t.me/c/1519198886/1062](https://t.me/c/1519198886/1062)&#x20;

\- CryptoBloks Blockchain Alerts [https://t.me/+IRu0UoMcGwlhZmMx](https://t.me/+IRu0UoMcGwlhZmMx)

\- CryptoBloks Blockchain Status [https://t.me/+OhLihPrdqH4xN2Ux](https://t.me/+OhLihPrdqH4xN2Ux)
