---
description: >-
  Libre chain has lots of dev resources and a supportive community of developers
  on Discord.
---

# Libre Developer Tools

### Libre Ecosystem Page

[https://chain.libre.org/apps](https://chain.libre.org/apps) has some open-source and mobile applications that show the power of integrating or building on Libre. To add your app, please join discord and let us know how you are using Libre.

You can find some open-source projects at the following repositories:

**Smart Contracts, Scripts, (this Gitbook), and more:**

{% @gitlab-files/gitlab-code-block %}

**DeFi Dashboard UI and API:**

{% embed url="https://gitlab.com/libre-tech" %}

**Other Tools and Scripts:**

{% embed url="https://github.com/edenia" %}

### Libre API Endpoints

Most Libre validators run open-access API endpoints separately from their validator nodes. There are several types of nodes that provide different services - producer, full-history, hyperion, v1, v2, p2p. Check out [Libre Chain Nodes](https://gitlab.com/libre-chain/libre-chain-nodes) for in depth technical info on running your own nodes.

**Mainnet endpoint monitor:**

{% embed url="https://libre.antelope.tools/endpoints" %}
Libre Mainnet Endpoint Monitoring
{% endembed %}

### Libre RPC API

Libre has a fully functional API for interacting with the chain and chain history. Create accounts, submit transactions, get balances, deploy smart contracts.

{% embed url="https://testnet.libre.org/v2/docs/static/index.html" %}
Swagger Docs for Libre API
{% endembed %}

### Javascript API&#x20;

Open-source Javascript API package for interacting with the RPC API. There are over 14k examples of this simple integration [available on Github](https://github.com/search?q=eosjs\&type=code).

{% embed url="https://www.npmjs.com/package/eosjs" %}
EOSJS Signing API for mobile and web integrations
{% endembed %}

### Local Libre Testnet for Development

There is a very simple tool for setting up a local chain dev environment located here on Github.

{% embed url="https://github.com/edenia/libre-local" %}

### Libre Link Signing Modules

Want to integrate Libre into your existing wallet? You can use the open-source Libre-Link and Libre-Link-Browser npm packages for authentication and execution of signing requests.

{% embed url="https://www.npmjs.com/package/@libre-chain/libre-link-browser-transport" %}

{% embed url="https://socket.dev/npm/package/@libre-chain/libre-link" %}
