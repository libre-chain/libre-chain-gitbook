# Table of contents

## The Libre Platform

* [Libre is a Decentralized Lending Marketplace](README.md)
* [Run A Libre Node](the-libre-platform/run-a-libre-node.md)
* [Validators](the-libre-platform/validators.md)
* [The Smart Contract Platform](the-libre-platform/building-on-libre/README.md)
  * [Building Smart Contracts on Libre](the-libre-platform/building-on-libre/building-smart-contracts-on-libre.md)
  * [Libre Developer Tools](the-libre-platform/building-on-libre/libre-developer-tools.md)
  * [Libre Testnet](the-libre-platform/building-on-libre/libre-testnet.md)
  * [API Docs](the-libre-platform/building-on-libre/api-docs.md)

## BitcoinFi on Libre&#x20;

* [DeFi](bitcoinfi-on-libre/defi-on-libre-dex.md)
* [Glossary](bitcoinfi-on-libre/validators.md)

## The LIBRE Reward Coin

* [A reward system for validators and stakers](the-libre-reward-coin/a-reward-system-for-validators-and-stakers.md)
* [Supply](the-libre-reward-coin/supply.md)
* [Mint Rush](the-libre-reward-coin/mint-rush.md)
* [Spindrop](the-libre-reward-coin/spindrop/README.md)
  * [How to Claim the LIBRE Spindrop](the-libre-reward-coin/spindrop/how-to-claim-the-libre-spindrop.md)
  * [Eligibility Requirements for the LIBRE Spindrop](the-libre-reward-coin/spindrop/eligibility-requirements-for-the-libre-spindrop.md)

## ACCOUNTS AND WALLETS

* [LIBRE Account Names](accounts-and-wallets/libre-account-names/README.md)
  * [Cryptographic Keys](accounts-and-wallets/libre-account-names/cryptographic-keys.md)
* [Creating Multiple Accounts](accounts-and-wallets/creating-multiple-accounts.md)
* [Bitcoin Libre Wallet](accounts-and-wallets/bitcoin-libre-wallet.md)
* [Anchor Wallet](accounts-and-wallets/anchor-wallet/README.md)
  * [Creating a Libre MainNet Account in Anchor](accounts-and-wallets/anchor-wallet/creating-a-libre-mainnet-account-in-anchor.md)
  * [Creating a Libre Testnet Account in Anchor](accounts-and-wallets/anchor-wallet/creating-a-libre-testnet-account-in-anchor.md)
  * [Getting Testnet LIBRE Coins](accounts-and-wallets/anchor-wallet/getting-testnet-libre-tokens.md)
* [Ledger Hardware Wallet](accounts-and-wallets/ledger-hardware-wallet.md)

## EARN

* [Staking](earn/staking.md)
* [Referrals](earn/on-chain-referrals.md)
* [Farming](earn/farming.md)
* [Become a Validator](earn/become-a-validator.md)
* [DAO Tax](earn/dao-tax.md)
* [Libre Lightning Provider](earn/libre-lightning-provider.md)

## Governance

* [Voting Power](governance/voting-power.md)
* [Validator Election](governance/validator-election.md)
* [Libre DAO](governance/dao.md)
* [Libre Governance Docs](governance/libre-governance-docs/README.md)
  * [Libre Blockchain Operating Agreement (LBOA)](governance/libre-governance-docs/libre-blockchain-operating-agreement-lboa.md)
  * [Block Producer Agreement](governance/libre-governance-docs/block-producer-agreement.md)
  * [Block Producer Minimum Requirements](governance/libre-governance-docs/block-producer-minimum-requirements.md)
* [Libre Mainnet Multisig Gov](governance/libre-mainnet-code-update-proposals/README.md)
  * [Oct 2024](governance/libre-mainnet-code-update-proposals/oct-2024.md)
  * [July 2023](governance/libre-mainnet-code-update-proposals/july-2023.md)
  * [June 2023](governance/libre-mainnet-code-update-proposals/june-2023.md)
  * [April 2023](governance/libre-mainnet-code-update-proposals/april-2023.md)
  * [March 2023](governance/libre-mainnet-code-update-proposals/march-2023.md)
  * [February 2023](governance/libre-mainnet-code-update-proposals/feb23stake.md)
  * [December 2022](governance/libre-mainnet-code-update-proposals/dec22updates.md)

## Technical Details

* [Libre Core Constants](technical-details/libre-core-constants.md)
* [Mint Rush Details](technical-details/mint-rush-details.md)
* [Staking Details](technical-details/staking-details.md)
* [Verifying Code Updates](technical-details/verifying-code-updates/README.md)
  * [Deployed Smart Contract Versions](technical-details/verifying-code-updates/deployed-smart-contract-versions.md)

## Cross-Chain Interoperability

* [Crosslink MPC TSS Bridge](cross-chain-interoperability/crosslink-mpc-tss-bridge/README.md)
  * [Bitcoin to Libre (Peg-In Process)](cross-chain-interoperability/crosslink-mpc-tss-bridge/bitcoin-to-libre-peg-in-process.md)
  * [Libre to Bitcoin (Peg Out)](cross-chain-interoperability/crosslink-mpc-tss-bridge/libre-to-bitcoin-peg-out.md)
* [Bitcoin Lightning Network](cross-chain-interoperability/bitcoin-lightning-network.md)
* [USDT / Ethereum](cross-chain-interoperability/usdt-ethereum.md)
* [Legacy pNetwork](cross-chain-interoperability/legacy-pnetwork.md)
* [Bitcoin Audit](cross-chain-interoperability/bitcoin-audit.md)
