---
description: Libre maximizes usability.
---

# Usability



**Accessibility** - Libre is accessible by anyone, anywhere in the world and it is conceptually accessible because it uses simple interfaces.

**Operability** - Libre is easy to operate. It does not require any gas tokens or special steps to use. It just works for normal people.

**Simplicity** - Libre is built with intuitive and simple interfaces - like the Bitcoin Libre wallet - which are very easy to learn and to share with others.

It's very simple for everyone to use and it's very easy to build on or integrate into any business to receive payments instantly with no transaction fees.

This is quite different from any other blockchain - all of the other blockchain solutions today require everyone to have a token to use the chain - which is a major blocker for new users.&#x20;

Libre was designed to be able to start using in just a few seconds. Just [download a wallet](https://chain.libre.org/wallets) and create an account, then try out using one of the [Libre applications](https://chain.libre.org/ecosystem).
