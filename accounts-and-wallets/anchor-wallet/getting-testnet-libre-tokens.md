---
description: How to get LIBRE tokens on testnet.
---

# Getting Testnet LIBRE tokens

Go to [https://libre-testnet.antelope.tools/faucet](https://libre-testnet.antelope.tools/faucet)

Enter your account name and click "Get Tokens"

<figure><img src="../../.gitbook/assets/image (29).png" alt=""><figcaption></figcaption></figure>

You will see a success notification.

<figure><img src="../../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

Next go to [https://testnet.libre.org/v2/explore](https://testnet.libre.org/v2/explore) - search your account and see that you got your tokens:

<figure><img src="../../.gitbook/assets/image (31).png" alt=""><figcaption></figcaption></figure>

If you add your account to Anchor wallet, you will see your account has LIBRE (testnet) tokens:

<figure><img src="../../.gitbook/assets/image (7).png" alt=""><figcaption></figcaption></figure>

