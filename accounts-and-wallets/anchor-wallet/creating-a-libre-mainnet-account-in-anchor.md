---
description: Everything you need to know about the Greymass Anchor desktop wallet.
---

# Creating a Libre MainNet Account in Anchor

Anchor is the leading desktop wallet for all Antelope chains. It is extremely reliable and trustworthy. You can download it at [https://greymass.com/en/anchor/](https://greymass.com/en/anchor/)\
\
Anchor can be used with LIbre in two ways:

* By importing an existing Libre account using a 51 char alphanumeric private key (beginning with a 5)
* By connecting to a Ledger Hardware Wallet.\


We'll cover the [Ledger setup here](../ledger-hardware-wallet.md).&#x20;

For now, lets focus on importing a key. For this, these are the steps:\
\
1\. Generate a private key and 12 word seed phrase from the Libre seed generator or Ian Colman Utility.  [https://explorer.libre.org/generate](https://explorer.libre.org/generate) - save **BOTH** the seed phrase and private key.\
\
2\. Insert that private key into the Libre account generator: [https://accounts.libre.org/](https://accounts.libre.org/)\
\
3\. Import the private key into Anchor.\
\
4\. Add the token addresses for pBTC and pUSDT as follows:\


`contract: BTC.ptokens`\
`token: pBTC`\
\
`contract: USDT.ptokens`\
`token: pUSDT`\
\
5\. At this point, you can see your balances in Anchor like this\
\


<figure><img src="../../.gitbook/assets/Screen Shot 2022-11-06 at 12.28.39 PM.png" alt=""><figcaption></figcaption></figure>

As long as you have saved the seed phrase, you will be able to import the account in Bitcoin Libre as well.
