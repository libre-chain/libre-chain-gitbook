---
description: Using the open-source Anchor wallet with Libre chain.
---

# Anchor Wallet

The [Anchor Wallet for Mobile and Desktop](https://greymass.com/en/anchor/) by [Greymass](https://greymass.com/en/) is an open-source and widely used software that runs on Windows and Mac.&#x20;

It supports multiple chains as well and enables storage of your private key encrypted on your device or inside of a [Ledger Hardware Wallet](../ledger-hardware-wallet.md). Anchor has an extremely strong track record of reliability.\
\
Greymass is also the developer of the Signing Request that is behind the way the LIBRE blockchain communicates with apps including the web dashboard.&#x20;

Download Anchor Wallet [https://greymass.com/en/anchor/](https://greymass.com/en/anchor/)

{% embed url="https://youtu.be/ATqLCPLK8Y4" %}
Enable Libre in Anchor
{% endembed %}

If you are running Anchor v1.3.7 or older, you can follow these instructions on adding Libre to Anchor Wallet (prepared by Cryptobloks) [https://cryptobloks.gitbook.io/using-anchor-wallet-with-the-libre-blockchain/](https://cryptobloks.gitbook.io/using-anchor-wallet-with-the-libre-blockchain/)&#x20;
