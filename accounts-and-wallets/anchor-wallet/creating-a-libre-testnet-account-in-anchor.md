---
description: Creating a Libre (Testnet) account using Anchor wallet.
---

# Creating a Libre Testnet Account in Anchor

First you will need to [download and install The Anchor Wallet (desktop edition)](./) to work with Testnet. Bitcoin Libre and Anchor mobile do not support Testnet at this time. Still, this will not be painful, we promise.

### **Enable Advanced Settings in Anchor**

Your anchor desktop should look like this. Click on Settings (top left)

<figure><img src="../../.gitbook/assets/1.png" alt=""><figcaption></figcaption></figure>

Change this to Display Advanced Settings.

<figure><img src="../../.gitbook/assets/2.png" alt=""><figcaption></figcaption></figure>

Now we see this.

<figure><img src="../../.gitbook/assets/3.png" alt=""><figcaption></figcaption></figure>

### Generate a Public / Private Keypair

{% hint style="info" %}
For advanced users, you can generate keys in in Anchor, or using the Ian Colman Utility - check out the [cryptographic-keys.md](../libre-account-names/cryptographic-keys.md "mention") section of these docs for more info. &#x20;
{% endhint %}

In your browser, go to: [https://explorer.libre.org/generate](https://explorer.libre.org/generate)&#x20;

Click "Generate Key Pair":

<figure><img src="../../.gitbook/assets/image (1) (1).png" alt=""><figcaption></figcaption></figure>

Then check the box after reading this warning:

<figure><img src="../../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

Copy the Public Key and Private Key from here - green and red sections below. Blue and brown sections are optional for testnet and may be confusing to save.

<figure><img src="../../.gitbook/assets/image (3).png" alt=""><figcaption></figcaption></figure>

## Create a Testnet Account

In a web browser, go to Faucet on Testnet [https://libre-testnet.antelope.tools/faucet](https://libre-testnet.antelope.tools/faucet)

Paste in the Public Key you created above and enter an account name (must be unique).

<figure><img src="../../.gitbook/assets/image (12).png" alt=""><figcaption></figcaption></figure>

Click “Create” and you will see a success notification on the bottom.

<figure><img src="../../.gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>



## **Add Your Testnet Account to Anchor**

**Follow the steps below - click "Setup an Account"**

<figure><img src="../../.gitbook/assets/13.png" alt=""><figcaption></figcaption></figure>

Choose Libre (Testnet)

<figure><img src="../../.gitbook/assets/14.png" alt=""><figcaption></figcaption></figure>

Click "Import an existing Account"

<figure><img src="../../.gitbook/assets/15.png" alt=""><figcaption></figcaption></figure>

Choose "Import Private Key"

<figure><img src="../../.gitbook/assets/Untitled.png" alt=""><figcaption></figcaption></figure>

Paste in the private key you got from the generator earlier - HINT: it begins with a "5".

<figure><img src="../../.gitbook/assets/Untitled 2.png" alt=""><figcaption></figcaption></figure>

Now click the @active permission and click the green button "Import Account"

<figure><img src="../../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

Congrats! Now your account is imported in Anchor and your private key is stored in Anchor.

Next, try to get some [Testnet LIBRE tokens](getting-testnet-libre-tokens.md).

