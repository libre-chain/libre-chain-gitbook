---
description: How to create (multiple) accounts and manage them on Libre blockchain.
---

# Creating Multiple Accounts

**1. Go to Libre's Seed Tool.** [**https://explorer.libre.org/generate**](https://explorer.libre.org/generate) [\
\
](https://iancoleman.io/bip39/)**2. Click "Generate Key Pair"**\
\
**3. Save public key, seed phrase, and private key in a secure place and DO not ever share the private key, seed phrase.**&#x20;

You will need all three of these, but especially the seed phrase for Bitcoin Libre and the the private key for Anchor or keosd.

Example:

`venture hat holiday scatter plunge choose bacon elephant stadium acquire dice alarm`

`PUB_K1_572k6vM7deK6chHXUdT6wRChjF6rosKb9Jjm2NK27Nb7ZV3eFWff9c`\
`5J3aSpTXi6WwNRWKUb6aNrsRMnu595dbtFudFFCajmx25FDBMEg`\
\
**4. Save seed phrase, public key, private key, and (optionally) extended private key**\
\
You will potentially need all three of these, but especially the seed phrase for Bitcoin Libre and the the private key for Anchor. You will also need the public key if you want to re-use the same private key on a different account name\
\
**5. Create an account using the Libre web tool** [**https://accounts.libre.org/**\
\
](https://accounts.libre.org/)You will need the public key and you can choose any 12 letter account name.\
\
Now, you can easily create as many accounts as you want using these 5 steps. For each account, use either Bitcoin Libre or Anchor and use the dashboard to login. The accounts can be funded with Bitcoin using the receive address on the dashboard or mobile app.\
\
Advantages of this versus single account on Bitcoin Libre:\
\
\- can move the same account between Anchor and Bitcoin Libre\
\- can pick the seed phrase using external mechanism (Ian Coleman’s tool is just one example)\
\- seed phrase can be done offline for max security\
\- can potentially move an account to a third party by swapping keys later on (staking stays the same)\
\- better to have lots of small accounts vs one big one (again, security). Anchor is good for this.

\
