---
description: Welcome to the advanced Libre user section! Congrats on finishing level 1.
---

# Libre Level 2 - Advanced

1. <mark style="background-color:orange;">How to optimize staking rewards.</mark>

Staking your LIBRE earns rewards - the rewards are higher the earlier you stake, and the longer you stake. You can get LIBRE using https://defi.libre.org/swap or in the Bitcoin Libre mobile app. Then you can stake by going to https://defi.libre.org/wallet and clicking the "New Stake" button. Compare the rewards for staking to the rewards for farming liquidity rewards which require no time lockup. See also: [Staking](../earn/staking.md)

2. <mark style="background-color:orange;">How to build or integrate Libre.</mark>

Libre is easy to integrate with a few short lines of code for authentication and payments. See the [Libre Dev Tools](broken-reference) section of this site.

3. <mark style="background-color:orange;">How to set up a Libre validator node.</mark>

Libre validators run the network, start here to learn more, please see[ Become a Validator](../earn/become-a-validator.md).

4. <mark style="background-color:orange;">How to earn LIBRE by providing liquidity (farm).</mark>

Another way to earn LIBRE is by providing liquidity to Libre swap liquidity pools which are permissionless and decentralized [https://defi.libre.org](https://defi.libre.org/). Go to [https://defi.libre.org/swap](https://defi.libre.org/swap) and click on Pool. See also [Farming](../earn/farming.md).

5. <mark style="background-color:orange;">How to create multiple accounts on Libre.</mark>

You can use the [Anchor Wallet](../accounts-and-wallets/anchor-wallet/) on Mac, Windows, or Linux to generate your own keys and create accounts on Libre. See the following section of this site for more details: [Creating Multiple Accounts](../accounts-and-wallets/creating-multiple-accounts.md).

6. <mark style="background-color:orange;">Libre Referrals</mark>

Libre is the first blockchain to build on-chain referrals where each account has a parent account. The parent account will earn a [bonus % of the rewards](../earn/on-chain-referrals.md) when a Libre user earns LIBRE or when they earn SATS from spinning the wheel in Bitcoin Libre. Your username is your referral code and there is a special link you can use in Bitcoin Libre to refer others to the mobile app.
