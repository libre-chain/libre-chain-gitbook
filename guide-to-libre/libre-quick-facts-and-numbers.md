---
description: Quick facts and numbers about the Libre blockchain.
---

# Libre - Quick Facts and Numbers

### <mark style="background-color:orange;">**Libre by the Numbers**</mark> &#x20;

470k accounts

500x Faster than Bitcoin

4000 transactions per second

0.05s Transaction Speed

0.01% Ecological Impact&#x20;

$0 Transaction Costs

### <mark style="background-color:orange;">Libre is the Trifecta</mark>

Scalable, Decentralized, Secure

### <mark style="background-color:orange;">Libre Quick Facts</mark>&#x20;

Libre uses Pegged Assets on the Libre chain (Bitcoin, Tether, Ordinals, BRC20)

Libre is a Sidechain (scaling layer) for the Bitcoin Blockchain and a bridge between Bitcoin and Ethereum

Libre Uses Simple-Name Addresses with Multi-Sig & Advanced Permissions

Libre's Bridging is run by pNetwork - Secure Interoperability between Chains

Libre is built on Antelope.io - Fast, Secure, User-Friendly Web3 Products

Libre is the first sidechain to integrate with The Bitcoin Lightning Network&#x20;

Libre's consensus is Delegated Proof-of-Stake - An Evolution in Energy Efficiency&#x20;

Libre is a Fair Launch chain - Free of Outside Funding or Concentration of Ownership

### <mark style="background-color:orange;">Libre Active Development Efforts</mark>

Bridging/Connecting Bitcoin and Ethereum in the Superfast Bitcoin Libre Wallet and Defi Dashboard

Building the First Ordinals Marketplace featuring Wrapped Ordinals (also superfast)

Join us in Libre Cafe for Basic Education. Informal, Inclusive.&#x20;

### <mark style="background-color:orange;">Problems Libre is Solving</mark>

Scaling Bitcoin by making transactions fast and free within our 2nd Layer (Sidechain).&#x20;

Making Ordinals Fast, Easy and Programmable.

Creating Easy-to-Use Products that are intuitive and easily adoptable.&#x20;
