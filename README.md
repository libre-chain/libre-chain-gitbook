---
description: >-
  Libre is a decentralized two-way marketplace for lending and borrowing, where
  anyone can permissionlessly provide liquidity and borrow against Bitcoin
  collateral.
---

# Libre is a Decentralized Lending Marketplace

### Summary

Libre is a decentralized, Bitcoin-first marketplace that enables secure, transparent borrowing and lending. With no centralized intermediaries, no speculative risks, and a predictable yield model, Libre empowers Bitcoiners to unlock the full potential of their assets while ensuring full self-custody and transparency.

### A Permissionless Way to Borrow Against Bitcoin

Libre allows anyone, anywhere to take out a term loan at a fixed rate using Bitcoin as the collateral. The Bitcoin is moved to a dedicated address controlled by the Crosslink MPC TSS nodes of the chain itself, and untouched until the loan is either payed back or a liquidation process has started, and the cure period has elapsed. See [crosslink-mpc-tss-bridge](cross-chain-interoperability/crosslink-mpc-tss-bridge/ "mention") for more information.

* **No rehypothecation**: Bitcoin remains untouched until the loan is repaid or liquidation occurs.
* **No taxable events**: Unlike wrapped Bitcoin alternatives (e.g., WBTC), there’s no token conversion. Borrowers can reclaim their exact Bitcoin after repaying their loan.

Unlike wrapped Bitcoin alternatives, there is no taxable event or token conversion (example: BTC to WBTC). Your Bitcoin is staked in your vault and you can borrow against the value. Once you pay you back your loan, you can take your Bitcoin out of the vault. You get back your Bitcoin, not someone else's because there is no pool, no commingling, no rehypothecation. You can easily audit it any time by looking at the balance in your vault's Bitcoin address.

### No Liquidations in the Middle of the Night

Loans taken against Bitcoin follow a fair warning system:

1. At **70% Loan-to-Value (LTV)**: Loan enters a **pre-warning state**.
2. At **80% LTV**: Loan enters a **warning state**, and borrowers have **72 hours** to either:
   * Add more Bitcoin as collateral.
   * Pay down their USDT loan.

If the LTV is not reduced within 72 hours, Bitcoin is liquidated **programmatically by smart contracts** to cover the loan, fees, and interest. This ensures no sudden liquidations occur during off-hours, giving borrowers time to act.

### Predictable Yield for USDT Lenders

USDT liquidity providers can choose from multiple pools of liquidity with different terms and rates - example the 30 day pool has a rate of 6% and a 365 day pool has a rate of 13% - new pools can also be created by anyone **all loans backed by Bitcoin collateral**.&#x20;

Libre’s decentralized system ensures:

* **Full transparency**: All loans are fully collateralized.
* **No speculative risks**: Funds are not exposed to trading, derivatives, or third-party lending.

[Broken link](broken-reference "mention")

### Fully Decentralized and Secure

* **Fast block confirmations** compared to Bitcoin’s network.
* **Full transparency** with tools and APIs to audit:
  * Bitcoin collateral in vaults.
  * LIBRE supply.
  * Validator performance.

Libre uses **Delegated Proof of Stake (DPOS)** consensus with 21 active validators, providing:

**Fast and Transparent Consensus**

* Validators and standby nodes ensure **no single point of failure**.
* Libre governance is handled by a **Decentralized Autonomous Organization (DAO)**, where LIBRE stakers vote on network decisions.

Anyone can contribute to the network by [running a **Libre node**](the-libre-platform/run-a-libre-node.md). Validators process transactions, produce blocks, and secure the system while earning **LIBRE token rewards**.

Libre has a more decentralized distribution of block production which can be visualized [here on this dashboard](https://libre-testnet.antelope.tools/block-distribution).

<figure><img src=".gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

### Built for Bitcoiners, By Bitcoiners

Libre was created to enhance Bitcoin’s utility in DeFi while adhering to first principles:

* **No central control**: Assets are secured by MPC nodes and smart contracts, with no custodianship.
* **Self-custody**: Borrowers and lenders retain control of their assets.
* **Human-readable account names**: Simplify interactions with names like “alice,” backed by a BIP 39 seed phrase.

Libre ensures that value remains on-chain as native Bitcoin while providing a **parallel DeFi layer** for liquidity pools, smart contracts, and trading.

### How Libre Differs from Celsius and Similar Platforms

Libre isn’t just another “earn yield on Bitcoin” platform—it’s a **secure, decentralized marketplace** that avoids the risks and pitfalls of centralized models like Celsius:

1. **No Risk to Bitcoin Collateral**
   * Centralized platforms like Celsius **rehypothecate Bitcoin** or use risky financial instruments like options to generate yield.
   * **With Libre**: Bitcoin stays in a dedicated vault, untouched and transparent.
2. **True Self-Custody**
   * Platforms like Celsius or Coinbase take full custody of your Bitcoin, leaving you dependent on their solvency.
   * **With Libre**: Bitcoin collateral remains under the borrower’s control in a dedicated, auditable vault.
3. **Fixed Yield Model**
   * Celsius and similar platforms often use speculative methods to generate variable yields.
   * **With Libre**: USDT lenders earn a predictable **fixed yield** sourced directly from loan interest.
4. **Transparent and Non-Speculative**
   * Libre avoids risky activities like leveraged trading or derivatives. All loans are backed by Bitcoin and secured by smart contracts.
5. **Your Bitcoin Stays Your Bitcoin**
   * Unlike centralized platforms, Libre ensures that the exact Bitcoin deposited as collateral is returned upon loan repayment.

### LIBRE has a coin, but you don't need it to use Libre.

The LIBRE coin has 2 purposes:

1\. To fund development of the Libre platform&#x20;

2\. To allow stakeholders to exchange value by contributing to the DAO via [mining](https://defi.libre.org/mining)

In order to incentivize independent validators to run the chain, provide peg-in and peg-out services in a sharded fashion, the LIBRE coin operates as a reward system, accruing some value from the DeFI and lending ecosystem.&#x20;

Stakeholders use their coin to vote on validators they trust will keep the system honest. In exchange, when a peg-in or peg-out occurs, or when a loan is paid back or liquidated, the staked LIBRE holders receive wrapped Bitcoin.

Libre is in this way a Bitcoin-based ecosystem that draws Bitcoin from parts of the ecosystem, and re-directs that Bitcoin to other parts of the ecosystem in order to keep it working in a decentralized way.

### LIBRE Coin Rewards in BTC

The **LIBRE token** powers the ecosystem and rewards network participants:

* **LIBRE Stakers**: Earn **Bitcoin rewards** from loan repayments, liquidations, and DeFi activity.
* **Validators**: Earn LIBRE tokens for maintaining network security and processing transactions.
* **DeFi Earnings**: A &#x31;**% spread** from borrower interest is converted to Bitcoin and distributed to LIBRE stakers programmatically.
