---
description: Become a validator (run a node) on Libre to earn LIBRE.
---

# Become a Validator

Anyone can become a Libre validator to earn LIBRE for processing Libre chain transactions and putting those into blocks.&#x20;

Anyone can also run a node that synchronizes the chain and provides API services by peering with any of the [existing network nodes](../the-libre-platform/validators.md).

The first step is to get a node running on the Libre Testnet following the [guide here on Gitlab](https://gitlab.com/libre-chain/libre-chain-nodes).&#x20;

The Testnet [Telegram](https://t.me/+l-Un\_5tmHzZkNzhh) and [Discord](https://discord.com/invite/asJNsB6sYJ) are good places to begin as a validator and get support with setting up your node. Good vibes only please.

For information about Validator Pay see the section of this site [validators.md](../the-libre-platform/validators.md "mention").
