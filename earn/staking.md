---
description: LIBRE staking and rewards explained.
---

# Staking

LIBRE must be "staked" to engage in governance - voting for validators and DAO proposals. In exchange for securing the network, LIBRE holders receive staking rewards in LIBRE. These staking rewards begin high and decrease over time.

\
The staking calculation ensures two behaviors:

**`Staking Earlier earns more`**

**`Staking Longer earns more`**

\
The return (APY) will be based on the staked duration and the date you stake. The longer you stake, the higher the APY and the earlier you stake, your APY will be higher as well. All of the values are stored on-chain as defined in the [Staking Table](../technical-details/staking-details.md).

### LIBRE Staking Logic <a href="#libre-staking-logic" id="libre-staking-logic"></a>

You choose the amount of LIBRE to stake and the number of days your LIBRE will be staked.

* The earlier you stake and the longer you stake, the more your yield will increase
* You must stake for at least 1 day - can stake up to 4 years
* If you want to claim your LIBRE before the Payout Date is reached, you can Emergency Unstake for a 20% loss on your initial stake and you will not earn any of the accrued interest
* 100% of mint rush LIBRE will be staked on day 30 for at least 30 days

Staking LIBRE can be done via [https://dashboard.libre.org](https://dashboard.libre.org) or by sending LIBRE using any wallet to `stake.libre` with the memo "stakefor:DAYS" - for example, if you want to stake 10,000 LIBRE for 365 days, you would send 10000.0000 LIBRE with a memo of "stakefor:365."

### LIBRE Staking Graph <a href="#libre-staking-graph" id="libre-staking-graph"></a>

The Maximum Initial APY of 200% (Beta0) trends towards the Maximum Terminal APY of 30% (BetaT) over 740 days (see [Libre Core Constants](../technical-details/libre-core-constants.md)). These values are coded into in the open source staking contract, they cannot be changed by the DAO. For more information, see[ Staking Details.](../technical-details/staking-details.md)

<figure><img src="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2Fy3YpEIcCKWuxnq3SuFgE%2Fuploads%2FolRSgGp3nn6IaNxWdDmj%2Fimage.png?alt=media&#x26;token=1202c629-8f13-4e5a-9c6a-58c1b0a99b52" alt=""><figcaption><p>Maximum APY decreases daily.</p></figcaption></figure>

{% content-ref url="../technical-details/staking-details.md" %}
[staking-details.md](../technical-details/staking-details.md)
{% endcontent-ref %}
