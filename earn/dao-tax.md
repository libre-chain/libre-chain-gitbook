---
description: DAO tax is applied to all claims of staked LIBRE.
---

# DAO Tax

Staking returns are inherently built into the Libre blockchain. This is inflationary by design, but becomes less inflationary over time due to the halvings related to validator rewards and LIBRE mining.&#x20;

It is also inflation that is generated directly out of code -- so that, from a user's perspective **if they are expecting a 10% APY, they will be guaranteed a 10% APY in unit terms**.&#x20;

In other words, new tokens are minted systematically for every stake, as opposed to generated as a whole for the chain and then apportioned to stakers.\
\
With this in mind, the token economics have been designed that every "mint" of LIBRE tokens corresponding to a stake results in a 10% mint to the DAO.&#x20;

You can think of this as a "**sustainability tax**" for community projects which include subsidizing wallets, audits, development, and even community growth / marketing.

Every spend from the DAO must be voted on by a majority of the overall [Voting Power](../governance/voting-power.md) and approved by 2/3+1 of the active validators.&#x20;

Voting Power is based on the amount of LIBRE that a user has actively staked.&#x20;

This will be constantly recalculated by the [DAO smart contract](https://gitlab.com/libre-chain/btc-libre-governance).\
\
