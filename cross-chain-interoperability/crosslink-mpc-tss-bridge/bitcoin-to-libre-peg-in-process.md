---
description: >-
  Libre Crosslink’s peg-in process allows users to securely bridge Bitcoin onto
  the Libre blockchain, converting Bitcoin into pegged BTC tokens that can be
  used within the Libre ecosystem.
---

# Bitcoin to Libre (Peg-In Process)

Here’s a step-by-step breakdown:

#### **1. Account Creation on Libre**

First, you need to create an account on the Libre blockchain. This account will be linked to any Bitcoin addresses you generate through the peg-in process.

#### **2. Requesting a Bitcoin Address from `x.libre`**

To begin, request a Bitcoin address by interacting with the `x.libre` bridge contract. This address will be generated in the **`bc1q...` format** (Bech32) and will be linked to your Libre account for the peg-in process.

#### **3. Smart Contract Entry (`pending` scope)**

Your request is recorded in the `x.libre` smart contract, where it is marked with the status `"pending"`. This contract table is monitored by the MPC nodes.

#### **4. MPC Nodes Monitor and Generate Key Shards**

MPC nodes detect the request and initiate the process of generating shards of the Bitcoin private key using the TSS protocol. Each node computes part of the private key independently.

#### **5. Signing and Address Generation**

Once the required number of nodes collaborate and sign using their key shards, the Bitcoin address (`bc1q...`) is generated. The smart contract status is updated to `"completed"`.

#### **6. Bitcoin Address Displayed in the UI**

The newly generated Bitcoin address is shown in your Libre account’s UI. You can now send Bitcoin to this address.

#### **7. MPC Nodes Monitor for Incoming Bitcoin**

MPC nodes, each running a Bitcoin full node, monitor the Bitcoin blockchain for transactions sent to the `bc1q...` address. When Bitcoin is received, the nodes independently confirm the transaction.

#### **8. Associating the Transaction with Your Libre Account**

The `x.libre` contract tracks which Libre account is linked to each Bitcoin address, ensuring that the received Bitcoin is tied to the correct user.

#### **9. Partial Signing to Issue BTC on Libre**

Once Bitcoin is received, the MPC nodes generate a partially signed Bitcoin transaction (PSBT) to mint an equivalent amount of pegged BTC on the Libre blockchain.

#### **10. Broadcast and BTC Issuance on Libre**

Once enough nodes sign the PSBT to meet the required threshold, the transaction is broadcast on the Libre blockchain, and the pegged BTC tokens are issued to your Libre account. The Bitcoin you sent is now represented as BTC on Libre, ready for use.
