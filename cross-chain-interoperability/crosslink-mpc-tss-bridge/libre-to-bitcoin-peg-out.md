---
description: >-
  When you want to move Bitcoin back from Libre to the Bitcoin network, the
  peg-out process is simple and decentralized.
---

# Libre to Bitcoin (Peg Out)

Here's how it works:

#### **1. Send BTC on Libre to `x.libre` with a Memo**

To initiate the peg-out, send your BTC (pegged Bitcoin on Libre) to the `x.libre` contract. In the memo field of the transaction, specify the Bitcoin address (formatted as `bc1q...`) where you want to receive the Bitcoin.

#### **2. Transaction Processing by MPC Nodes**

The `x.libre` contract records the peg-out request, and the MPC nodes begin processing it. Each node independently verifies the request and prepares to sign the outgoing Bitcoin transaction.

#### **3. Fee Deduction**

A small **1% fee** (capped at 1,000,000 SATs) is deducted from the transaction. This fee covers both Bitcoin network transaction costs and compensation for the bridge nodes.

#### **4. MPC Nodes Sign the Bitcoin Transaction**

Each MPC node independently generates a partial signature for the Bitcoin transaction using TSS. When the required number of nodes sign the transaction, the signatures are combined to form the final transaction.

#### **5. Broadcasting the Bitcoin Transaction**

Once the necessary threshold of signatures is met, the transaction is broadcast to the Bitcoin network. The Bitcoin will be sent to the specified `bc1q...` address.

#### **6. Bitcoin Received**

The Bitcoin transaction is confirmed on the Bitcoin blockchain, and the recipient address (`bc1q...`) will receive the Bitcoin. The peg-out process is now complete.
