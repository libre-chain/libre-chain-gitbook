---
description: A Truly Decentralized Bridge to and from Bitcoin
---

# Crosslink MPC TSS Bridge

Libre Crosslink is the most advanced and decentralized TSS MPC bridge available today. Unlike previous bridges that rely on centralized APIs and databases, Libre ensures full decentralization by:

* Running **full Bitcoin and Libre nodes** on each MPC node.
* Storing all critical information **publicly on the Libre blockchain** via smart contracts (example `x.libre`).
* **No centralized components**, making the system tamper-resistant, transparent, and secure.

Whether you're pegging Bitcoin into Libre or moving it back out to the Bitcoin network, the entire process is driven by decentralized MPC nodes and governed by on-chain smart contracts, ensuring that your Bitcoin is handled with maximum security and decentralization.
