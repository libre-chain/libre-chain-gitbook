---
description: Automatic bridging of pNetwork has ended.
---

# Legacy pNetwork

The pNetwork stopped supporting Libre on Oct 1, 2024. Redemptions are all handled manually now. There were over 100 BTC on Libre via pNetwork - there are approximately 2.2 BTC remaining.&#x20;

{% hint style="info" %}
If you have less than 200,000 SATS (0.0002 BTC) then you can use [Bitcoin Lightning](bitcoin-lightning-network.md) to bridge your PBTC out of Libre. Both [Bitcoin Libre mobile and browser extension](https://www.bitcoinlibre.io) support Lightning Payments. PBTC Lightning will be supported until Jan 1, 2025.&#x20;
{% endhint %}

### To redeem your PBTC, follow these steps:

The redemption process requires you to do a transaction on Libre and submit that transaction hash to pNetwork support.

1\. Open the Bitcoin Libre mobile app.

2\. Select Bitcoin (BTC) and tap ‘Receive’ to copy your BTC address.

3\. On a computer, navigate to [legacy.libre.org](https://legacy.libre.org).

4\. Click ‘Connect Wallet’ and scan the displayed QR code using the Bitcoin Libre mobile app.

5\. Locate PBTC in your asset list and click ‘Send’ on the right.

6\. Paste the BTC address copied in step 2 and select ‘Send All’.

7\. After the transaction, note the transaction ID displayed at the bottom of the screen or find it on [libreblocks.com](https://libreblocks.com) by searching your account name.

8\. Visit [pNetwork Support](https://support.p.network/) and choose the email method - **DO NOT SEND ANY KEYS or SEED PHRASES.**

9\. Complete the form with the following details:

• Token: PBTC

• Operation: PegOut

• Native Chain: Bitcoin

• Host Chain: Libre

• Transaction Hash: Paste the transaction ID from step 7

• Comments: Leave blank

<figure><img src="../.gitbook/assets/image (33).png" alt=""><figcaption></figcaption></figure>

Redeeming PUSDT follows a similar process, but you choose PUSDT instead of PBTC.
