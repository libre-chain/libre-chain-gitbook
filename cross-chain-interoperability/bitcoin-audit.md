---
description: Anyone can audit to make sure that the BTC on Libre is 100% backed by Bitcoin.
---

# Bitcoin Audit

Every Libre account has it's own Bitcoin address that can be viewed on the x.libre smart contract.

If you send Bitcoin to that address, you will receive a "wrapped" Bitcoin on Libre.

To audit the Libre Crosslink Bitcoin - use this script:&#x20;

[https://gitlab.com/libre-chain/crosslink-bridge-bitcoin-audit](https://gitlab.com/libre-chain/crosslink-bridge-bitcoin-audit)

{% hint style="info" %}
Each loan vault also has it's own Bitcoin address that is stored in vault.libre - running with the same security and transparency.&#x20;



You can use the same script above by changing the "code" from x.libre to vault.libre to verify all vaults.
{% endhint %}

