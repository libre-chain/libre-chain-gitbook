# USDT / Ethereum

Libre nodes process USDT peg-in and peg-out transactions.&#x20;

The peg-in is a 2 step process where you must approve an amount to be bridged and then allow that amount to be transferred by the bridge contract.

Currently, this supports Ethereum - but will soon also support Polygon (MATIC) and more.

