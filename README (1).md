---
description: Fast Bitcoin and Tether + Ordinals
---

# First Principles

### LIBRE is based on First Principles created by Satoshi Nakamoto for the Bitcoin Whitepaper.

### Peer-to-Peer Electronic Cash

Libre was designed to enable the features of the Bitcoin Libre mobile wallet - the first truly decentralized, feeless digital cash mobile app. With Bitcoin Libre you can send Bitcoin, Tether, or any other tokenzied asset on Libre to anyone with a Libre account with no fees. All transactions are verified by the [Libre Validators](the-libre-blockchain/validators.md) and are 100% on-chain and transparent.

### Decentralization

The third principle is Decentralization. Libre is designed to have no single point of failure. There are multiple wallets, validators, standby validators, APIs, and multi-sig permissions on all important system contracts. Compared to Bitcoin and Ethereum mining pool concentrations, Libre has a more decentralized distribution of block production which can be visualized [here on this dashboard](https://libre-testnet.antelope.tools/block-distribution).

![](<.gitbook/assets/image (4).png>)

{% hint style="info" %}
Note this is a testnet dashboard, but mainnet has a similar distribution, the mainnet dashboard for this metric is not live yet.
{% endhint %}

### Consensus

The next principle of Nakamoto is 'Truth by Consensus'. This occurs through an immutable peer-to-peer protocol, called 'trustless' because there is no corruptible intermediary upon whom exchanges must depend. Libre's Blockchain has 21 Validators that use DPOS (Delegated Proof of Stake) consensus mechanism which enables faster confirmations compared to the Bitcoin Network. Transparency is fundamental to consensus. There are simple interfaces and APIs to validate the amount of Libre, the amount of BTC backing Bitcoin on Libre etc. Validators are chosen by LIBRE coin holders who stake Libre. The strength of their vote depends on the time remaining in their stake - see “Voting Power” for more details. Libre is run by a DAO (Decentralized Autonomous Organization) using community consensus of token holders to  determine how funds are allocated.&#x20;

### Self Custody&#x20;

Libre is built for self-custody of assets. You retain full control of your assets and economic energy following the first principles of Blockchain. LIBRE is universally assessable with the freedom to do more than send and receive, but also allows your assets to work for you. You are in the drivers seat as no permission is needed from a third party intermediary with the use of our bridges, multi-sig (multiple signature) and MPC. We encourage safe storage of all your seed phrases and private keys as a self custodian. We also encourage you to move your assets back to native chains as often as possible - do your trading, transacting on Libre, but take your holdings back to Bitcoin and Ethereum as often as you can.



