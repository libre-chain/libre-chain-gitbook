---
description: The LIBRE Spindrop ended in January 2023.
---

# How to Claim the LIBRE Spindrop

The LIBRE coin spindrop was claimed, and could only be claimed, by going to spindrop.libre.org. Please verify the libre.org URL as airdrops such as this are frequently targets for phishing and other malicious attacks.\
\
When you went to the spindrop site, you would first be asked to verify your identity with a standard blockchain wallet (example: Phantom for Solana, MetaMask for Ethereum etc..). This did not, and should not, initiate a token transfer. Again, beware of scams -- **always check the URL matches** [**https://spindrop.libre.org/**](https://spindrop.libre.org/) \
\
Finally, once you authenticated with your wallet, the eligibility of your account was determined. This was a pre-determined "yes" or "no" depending on a set criteria for each blockchain at a historical snapshot date. In general, if your account has even a minimum amount of value in it the answer will be "yes" -- and you can continue to the spin stage. Note that **you can claim multiple times, once for each blockchain and account.**\
\
Once claimed, the next thing to do is spin. This will initiate a random number being selected and saved, and a certain amount of LIBRE available to spin. At this point you can simply enter a valid Libre account name to collect the LIBRE tokens. Again, since the random seed is stored, repeated spins of the wheel will yield the same result.
