---
description: >-
  The Libre Spindrop was an airdop of coins randomly decided by spinning a
  wheel.
---

# Spindrop

10,000,000 LIBRE was been allocated to be claimed via a broad-based airdrop.&#x20;

As of Oct 27, 2022 - only 110,000 LIBRE had been claimed.

The airdrop was available to qualified accounts on Solana, Ethereum, Proton, EOS, Telos, and WAX.&#x20;

Near the end of the in January 2023 all of the unclaimed coins were sent to the Libre DAO Treasury controlled by the LIBRE token holders and Libre Validators.

9,722,600 LIBRE was sent to the DAO on January 24, 2023 - you can view the transactions on the explorer link here:

{% embed url="https://www.libreblocks.io/address/drop.libre" %}

[eligibility-requirements-for-the-libre-spindrop.md](eligibility-requirements-for-the-libre-spindrop.md "mention")

[how-to-claim-the-libre-spindrop.md](how-to-claim-the-libre-spindrop.md "mention")
