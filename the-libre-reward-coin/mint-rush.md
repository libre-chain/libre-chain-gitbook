---
description: Libre Mint Rush Explained
---

# Mint Rush

From Dec 15, 2022 to Jan 14, 2022 the public launch of the LIBRE coin: the "Mint Rush" was completed. This was the fair launch of the governance coin LIBRE where there was 200M tokens distributed to anyone who contributed BTC during the Mint Rush time period. 100% of the BTC raised went into the AMM liquidity pool along with 200M LIBRE - owned by the Mint Rush contributors.

{% hint style="info" %}
**Impermanent Loss Exposure -** It should be known that providing liquidity to an AMM is not risk free and the initial liquidity is likely to change based on trading activity. Anyone who contributes to the Mint Rush is subject to "impermanent loss" in the AMM pools.
{% endhint %}



{% hint style="info" %}
**Mint Rush Staking** - the stake date for all contributions was at the end of the Mint Rush on January 14, 2023. If you staked for 1 year, your stake will end on Jan 14, 2024.&#x20;

This is not 1 year from the date of contribution. This is outlined in the UI when you make a contribution and it is coded into the smart contract code of stake.libre.
{% endhint %}

### Mint Rush Logic  <a href="#mint-rush-logic" id="mint-rush-logic"></a>

Contributors chose the amount of BTC to contribute, and the days that LIBRE will be staked. The earlier you contributed, the more of a bonus you will received on your initial stake . You must have staked for at least the duration of the mint rush - and you could have staked up to 4 years.&#x20;

At the end of the mint rush - LIBRE was distributed to you based on this formula ( your contribution / total contributions ) \* total LIBRE minted. 100% of Mint Rush coins are staked on day 1 after distribution. The earlier you contribute to the mint rush, the higher your staking multiplier was. Every day the staking multiplier decreased at a rate of (day of mint rush / total days in mint rush) - since the Mint Rush was 30 days long, on day 15, the bonus was 1/2.&#x20;

Example below shows the APY available on each day of the Mint Rush for stakes of&#x20;

* 365 days (blue)
* 180 (red)
* 90 (yellow)&#x20;
* 30 (green).&#x20;

This example shows that the Mint Bonus Multiplier was 2x and the Mint Rush Period was 30 days. If you contributed to the Mint Rush on day 0, the maximum APY is 400% for a 365 day stake commitment, 286% for 180 day stake, 208% for 90 day stake, 128% for 30 day stake. If you contributed to the Mint Rush on day 30/30, the maximum APY is 200% for a 365 day stake, 143% for 180 day stake, 104% for 90 day stake, and 64% for 30 day stake.

<figure><img src="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2Fy3YpEIcCKWuxnq3SuFgE%2Fuploads%2Fl7KLxuKLU4dZX086JbeB%2Fimage.png?alt=media&#x26;token=68d3c737-2259-40fd-8ebd-20026c5dfbf9" alt=""><figcaption><p>Mint Rush Bonus based on Stake Duration</p></figcaption></figure>

### AMM Liquidity Tokens

In exchange for contributing to the Mint Rush, each account will have an ownership stake of the LP tokens in the liquidity pool. These LP tokens will be distributed daily for 365 days using a smart contract.

### Design

The Mint Rush is designed to distribute the majority of tokens in a fair way to the entire ecosystem, in exchange for funding the LIBRE / BTC AMM liquidity pool. This also allows LIBRE to find an initial price over the duration of the Mint Rush. At the end of the Mint Rush, LIBRE will be distributed by running the "mintprocess" action on the Mint Rush smart contract.

### Participation

To participate in the Mint Rush, users needed to send pegged BTC ([PBTC](broken-reference)) to a smart contract. A fixed amount of 200M LIBRE tokens was distributed pro-rata to all Mint Rush participants. 100% of the proceeds will go into a liquidity pool on the LIBRE AMM.\
\
As an incentive to early participation, users were given a declining yield bonus that applies to their initial mint rush stake. So, the price for all participants was be the same (and discoverable at the end of the mint rush as the ratio of Total Mint Rush Libre / BTC contributed), but the staking returns will vary depending on timing of the contribution.

The math for calculating the Mint Rush bonus can be found in the [Mint Rush section of the whitepaper](../technical-details/mint-rush-details.md). For example, staking on Day 1 of the Mint Rush got you a 2x bonus on the Mint APY (400%) on your staking rewards. Day 15 bonus is calculated as ( 15 / 30 \* 2x) = 1x (200%).&#x20;

The start date, duration, maximum mint bonus, and amount of LIBRE is hard coded into the staking contract.&#x20;

### Referrals

Referring someone to the Mint Rush by using your referral link (https://app.libre.org/yourusername) was another way to participate. When you referred someone and they earn from staking rewards, you will earn 10% of a bonus on top of what they earned.

For example, if you referred someone and they contributed $1,000 to the Mint Rush on Day 0 and stake for 1 year, if they claim a value of $4,000 - you would earn $300 (10% of the $3,000 bonus they accumulated).

The referral system is [completely on-chain](../earn/on-chain-referrals.md) and hard-coded into the staking contract.

### Example Mint Rush Scenario

_This is meant for illustrative purposes only and this is not financial advice or a promise of future returns. Numbers have also been simplified to provide an example._

Bitcoin is at $20,000 and the Mint Rush raised a total of 50 BTC.&#x20;

Alice has contributed 1 BTC to the Mint Rush on Day 0 and has staked for 1 year.&#x20;

At the end of the Mint Rush, Alice receives 1/50th of the 200,000,000 LIBRE allocated to the Mint Rush = 4,000,000 LIBRE, which is staked at a rate of 400% for 365 days. She can claim this stake early for penalty or wait until the end of the staking period and claim 16,000,000 LIBRE.

Alice also has a claim on 1/50th of the initial liquidity pool of 50 BTC and 200,000,000 LIBRE - that claim will be represented in "LP Tokens" to be distributed over a 365-day period.&#x20;

Assuming the price of LIBRE / BTC does not change and no trading has occurred in the pools, Alice will then have access to 1 BTC and 20,000,000 LIBRE (4,000,000 from the LP pool + 16,000,000 from the stake). In addition, the LP tokens will have accumulated some amount of trading fees from the AMM.&#x20;
