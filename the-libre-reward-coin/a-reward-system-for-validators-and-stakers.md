# A reward system for validators and stakers

In order to incentivize independent validators to run the chain, provide peg-in and peg-out services in a sharded fashion, the LIBRE coin operates as a reward system, accruing value from the DeFI and lending ecosystem. Stakeholders use their tokens to vote on validators they trust will keep the system honest. In exchange, when a peg-in or peg-out occurs, or when a loan is paid back or liquidated, the staked LIBRE holders receive wrapped Bitcoin.

Libre is in this way a Bitcoin-based ecosystem that draws Bitcoin from parts of the ecosystem, and re-directs that Bitcoin to other parts of the ecosystem in order to keep it working in a decentralized way.

