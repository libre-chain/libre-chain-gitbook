# Ordinals

Data Stored on Bitcoin. Digital Artifacts Inscribed forever into the Bitcoin Blockchain.&#x20;

3 Categories

Media - Image, Video, Sound, HTML Data

.SATS - Domains on Bitcoin. You pay only once, unlike .coms etc. You own it forever

BRC20 Tokens - A value creation protocol for developing utility and community
