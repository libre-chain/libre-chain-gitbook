# Building Smart Contracts on Libre

Libre meets developers where they are at. There is no need to learn a new language as Libre supports building in almost all modern and popular programming languages including C++, Typescript, Rust, Go, and Python.

There are online several online IDEs you can use to build on Libre Chain - so you do not have to install any software locally to get started.&#x20;

For example, any Antelope-based tools will work with Libre Chain, for example:

* [Rust Smart Contract Development Kit](https://github.com/uuosio/rscdk)
* [Python Smart Contract Development Kit](https://github.com/uuosio/pscdk)
* [Typescript Web IDE](https://protonide.com/)
* [AssemblyScript Smart Contract Development Kit](https://github.com/uuosio/ascdk)
* [Go Smart Contract Development Kit](https://github.com/uuosio/gscdk)
* [C++ Studio for Smart Contracts](https://app.eosstudio.io/) (web version may have bugs)
* [CLSDK - rapid testing framework](https://forums.eoscommunity.org/t/clsdk-and-cltester/3182)
  * [CLSDK Demo Repo](https://github.com/gofractally/demo-clsdk)

You can build smart contracts following this [collection of examples](https://github.com/blockmatic/antelope-contracts-list). Please start on Testnet - where the validators can help you with getting resources and access to the public API servers.&#x20;
