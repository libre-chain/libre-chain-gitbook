# DeFi

DeFi (Decentralized Finance) on Libre was built because there is a need for Layer 2 Blockchain Solutions for Bitcoin that go beyond send and receive transactions. To have a full alternative to the Fiat system that follows First Principles where self custody is inherent. You can now have your assets work for you.

This allows for system that is universally assessable, governed by code, with no intermediary for which you are required to ask for permission.

