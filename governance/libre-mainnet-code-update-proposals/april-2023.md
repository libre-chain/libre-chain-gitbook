---
description: Multisig and code updates for April 2023.
---

# April 2023

## Multisig 1/x - “systemupdate”

[https://msig.app/libre/quantum/systemupdate](https://msig.app/libre/quantum/systemupdate)

#### Summary

Updates eosio to reference voting power in stake.libre::power for choosing validators

This has been fully tested on testnet and the votes are calculated based on voting power - schedule updates perfectly. It has been running for a few weeks on testnet.

### Requested signers

all active producers

### Code Changes

**Repo:** [https://gitlab.com/libre-chain/libre-chain-contracts](https://gitlab.com/libre-chain/libre-chain-contracts)

**Path to WASM in repo:** &#x20;

* build/contracts/libre.system/libre.system.wasm

|             | Currently Deployed                                               | Proposed Update                                                  |
| ----------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- |
| Commit hash | fb8105fe7eefd63fa951a655b396a8c3cc305456                         | be8b30272344aa6d3383faab30967e6f1e32486f                         |
| sha256sum   | 42705132b34d90518a47341aee666cfb973f8cc6a0f2943c13dc5ce20a916769 | ef4bdc3c999da05d118ac8afefdcd8a67d6119963b8be16558545cd56444a4d7 |

**Files updated in this WASM:**

* contracts/libre.system/include/libre.system/libre.system.hpp
* contracts/libre.system/src/libre.system.cpp

**Code comparison (proposed to deployed):**

Note: We do not know the EXACT commit hash of the code deployed, so this is an estimation of the state of the code, but the ABI matches from this time - but we have downloaded the deployed WASM and updated a local network with the new WASM to verify.

You can verify the ABI by running this locally:

```
cleos -u https://lb.libre.org get abi eosio > eosio.abi

diff <(jq -S . eosio.abi) <(jq -S . ./build/contracts/libre.system/libre.system.abi )
```

Once this update is deployed, the action `migrate` will need to be run with `"max_steps": "1000"` until it fails. This will active the voting power calculations to use stake.libre::power for the ranking of validators.

## Multisig 2/x - “permsdomiscd”

[**https://msig.app/libre/libretech/permsdomiscd**](https://msig.app/libre/libretech/permsdomiscd)

#### Summary

There was a small issue affecting early beta testers of Bitcoin Libre where an old BTC address was cached. Unfortunately, months after she stopped using this account, Domi's new account on Bitcoin Libre used a cached BTC address that belonged to this account. This is a key reset for the account she lost "domiscd" which was created during beta testing. It has been verified 100% to belong to her and the BTC wallet where she sent from is also verified.

### Requested signers

all active producers

### Code Changes

none

## Multisig 3/x - “ratifygoverna”

[https://msig.app/libre/cryptobloks/ratifygoverna](https://msig.app/libre/cryptobloks/ratifygoverna)

#### Summary

Approves initial governance docs that can be viewed here: [libre-governance-docs](../libre-governance-docs/ "mention")

### Requested signers

all active producers

### Code Changes

**N/A**

## Multisig 4/x - “daogovfund”

[https://msig.app/libre/cryptobloks/daofunding](https://msig.app/libre/cryptobloks/daofunding)

#### Summary

Issues 100M LIBRE to dao.libre which can only be spent by 2/3+1 validator approval. This is covered in the [Libre Operating Agreement section 17](https://gitlab.com/libre-chain/libre-governance/-/blob/main/OperatingAgreement.md#17-libre-dao-additional-funding) and passed a DAO vote [here](https://dashboard.libre.org/dao/3yiz3avosj2n).

### Requested signers

all active producers

### Code Changes

**N/A**

