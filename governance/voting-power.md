---
description: Voting Power on Libre blockchain
---

# Voting Power

The Libre blockchain is governed by the LIBRE token which must be "staked" (aka locked) for a time in order to have some "skin in the game" when voting. Accounts are rewarded for staking by earning LIBRE whether or not they actually engage in governance.

Every Libre account that has [staked LIBRE tokens](../earn/staking.md) has a voting weight called "voting power" that determines the weight of that account's votes in the DAO and for voting on Validators.

There is a leaderboard visible on the dashboard at [https://dashboard.libre.org/dao/leaderboard](https://dashboard.libre.org/dao/leaderboard) and this is verifiable on-chain by using a block explorer and looking at the [stake.libre::power table](https://local.bloks.io/account/stake.libre?nodeUrl=http%3A%2F%2Flb.libre.org\&coreSymbol=LIBRE\&systemDomain=eosio\&hyperionUrl=http%3A%2F%2Flb.libre.org\&loadContract=true\&tab=Tables\&account=stake.libre\&scope=stake.libre\&limit=100\&table=power). &#x20;

This power is determined by the remaining days of each stake of LIBRE, up to a maximum of 1 year.

The formula to calculate the voting power (vp) is:

$$
vp = (staked LIBRE) * (1 + MIN(1,(stakeLength /  365))
$$

For example, if you have 1000 tokens staked with 365 days remaining, your voting power will be 2000. If you only have 10 days remaining, your voting power will be approximately 1027.39726.

{% hint style="info" %}
It's important to note that this formula formula is implemented in the [open-source taking contract](https://gitlab.com/libre-chain/staking-contract) and cannot be updated without a hard fork of the code.
{% endhint %}
