---
description: Validator elections on Libre blockchain
---

# Validator Election

Validators play an important role in securing the network and processing transactions. The network uses a delegated-proof-of-stake mechanism, where staked LIBRE tokens are used to elect validators. Validators are responsible for verifying the validity of transactions and creating blocks of transactions as well as running API servers for history and other functions.

To ensure the security and stability of the chain, it is crucial to have a minimum of 10 validators validating each block. If this threshold is not met, the block validation rule may fail, and the entire chain could come to a halt.

Any Libre account with staked LIBRE can vote for validators using their [voting power](voting-power.md).

{% hint style="info" %}
The process of selecting validators is significant. A comprehensive list of validators can be found on the dashboard at dashboard.libre.org. Each wallet has the ability to vote for a single validator. If you have a large number of tokens and want to distribute your voting power across multiple validators, you can create multiple accounts to achieve that.


{% endhint %}
