---
description: >-
  https://gitlab.com/libre-chain/libre-governance/-/blob/main/block-producer-agreement.md
---

# Block Producer Agreement

## Data Structures

```
Producer account name: {{producer}}
Producer signing key: {{producer_key}}
Producer ownership information: {owner:{name}, percentage_owned{percentage}, identity_provider_service {idprovider}, ID_hash {idhash}}
Candidate Name: {{producer_name}}
Candidate Website URL: {{producer_URL}}
Candidate Domicile Country (2-letter country code): {{entity_domicile_country}}
Candidate server location(s): {server:{location_name}, {server_location_country}, {server_location_latitude}, {server_location_longitude}}
```

## Human Language Terms <a href="#user-content-human-language-terms" id="user-content-human-language-terms"></a>

### 1. Intent of Action — "regproducer” <a href="#user-content-1-intent-of-actionregproducer" id="user-content-1-intent-of-actionregproducer"></a>

The intent of the \{{ regproducer \}} action is to register an account as a block producer candidate, to enumerate the obligations and rules of Block Producer candidacy and operation, and to inform producers about the penalties and penalty process for violation of these rules and obligations. This agreement is an addendum to the Libre Blockchain Operating Agreement (LBOA) and inherits its terminology and language.

### 2. Nomination <a href="#user-content-2-nomination" id="user-content-2-nomination"></a>

I, \{{producer\}}, hereby nominate myself for consideration as a Libre Blockchain Block Producer. This nomination includes the express agreement to all terms of this human-language contract by the block producer candidate entity and all of its owners.

### 3. Disclosure of Producer Key <a href="#user-content-3-disclosure-of-producer-key" id="user-content-3-disclosure-of-producer-key"></a>

If \{{producer\}} is selected to produce blocks by the Libre Blockchain Members under the voting terms of the "eosio" contract, I will sign blocks with \{{producer\_key\}} and I hereby attest that I will keep this key secret and secure. If I suspect my key has been compromised, I will call this contract action again to register a new secure key.

### 4. Disclosure of Entity and Server Information <a href="#user-content-4-disclosure-of-entity-and-server-information" id="user-content-4-disclosure-of-entity-and-server-information"></a>

I, \{{producer\}}, acknowledge that I have disclosed accurate information about my block producer entity and server location(s). Server location data is accurate to within two degrees of latitude and longitude. The country of domicile of my ownership entity, expressed as a 2-letter ISO code is \{{entity\_domicile\_country\}}. The location(s) or my block producer server(s) is \{{location\_name\}}, expressed as a 2-letter ISO code it is \{{server\_location\_country\}}. The location of my block producer server(s), expressed as latitude and longitude is \{{server\_location\_latitude\}}, \{{server\_location\_longitude\}}.

### 5. Penalty Enforcement via the “kickbp” Action of the "eosio" Contract <a href="#user-content-5-penalty-enforcement-via-the-kickbp-action-of-the-eosio-contract" id="user-content-5-penalty-enforcement-via-the-kickbp-action-of-the-eosio-contract"></a>

I, \{{producer\}}, acknowledge that if I fail to fulfill the obligations or violate the rules set forth in this contract, that the other Block Producers shall impose upon me the penalties enumerated for the corresponding violation. The vehicle for this enforcement shall be the "kickbp" action on the "eosio" contract using a multi-signature ("eosio.msig") contract to be executed by the Block Producers, using the "eosio@active" permission, which may be signed or executed by any Block Producer to allege a violation of the rules and present evidence of the violation. I will have a maximum of 50,000 blocks (approximately seven hours) to post a rebuttal to the evidence presented against me prior to other block producers voting. If a 2/3+1 majority of elected block producers votes that they are convinced the infraction did occur, then the enumerated penalty associated with the associated broken rule shall be imposed.

### 6. Obligation to Enforce Block Producer Rules <a href="#user-content-6-obligation-to-enforce-block-producer-rules" id="user-content-6-obligation-to-enforce-block-producer-rules"></a>

I, \{{producer\}}, acknowledge that at any time I serve as a Block Producer, I shall enforce the rules and associated penalties set forth in this human-language contract when another Block Producer or Standby Block Producer reports evidence of an alleged violation in the form of signing and, once signed by a sufficient number of Block Producers, executing the multi-signature action calling the "kickbp" action of the "eosio" contract. When the "kickbp" action is proposed by any Block Producer or Standby Block Producer, I have the obligation to assess all evidence presented both by the accuser and the accused and vote whether the evidence persuades me that the alleged infraction has occurred. I shall, within 100,000 blocks (approximately 14 hours) of the multi-signature action being first created in the case, cast a definitive yes or no vote via the multi-signature action calling the “kickbp” action or alternatively, posting to the chain a permanenet message stating I decline to sign the action. Failure to sign the multi-signature action calling “kickbp” or post a statement on the chain within 100,000 blocks shall place me in violation of the block producer minimum requirements until I sign, post a message of refusal to sign, or until the multi-signature action calling the "kickbp" action is enacted by a majority of 2/3+1 of all Block Producers. If I discover evidence of a violation of this human-language contract by another Block Producer or Standby Block Producer, I am obligated to create a multi-signature action calling the "kickbp" action of the "eosio" contract and provide evidence of the alleged violation as quickly as I may fully investigate and collect evidence.

### 7. Resignation and Removal for Inability to Perform Obligations <a href="#user-content-7-resignation-and-removal-for-inability-to-perform-obligations" id="user-content-7-resignation-and-removal-for-inability-to-perform-obligations"></a>

If \{{producer\}} is unable to perform obligations under this human-language contract I will resign my position by calling the "unregprod" action of the "eosio" contract. If \{{producer\}} fails to resign when unable to perform said obligations, \{{producer\}} shall be removed by automated contract or by actions of the remaining block producers. \{{producer\}} may be removed at any time when it fails to produce 15% or more of its assigned blocks in a rotation of the block producer scheduling routine, or any time when 1/3–1 of all Block Producers are failing to produce blocks on the current schedule and \{{producer\}} is the block producer with the highest number of missed blocks or the lowest percentage of Member votes among the group of block producers that is failing to produce blocks.

### 8. Objectively Valid and Invalid Blocks <a href="#user-content-8-objectively-valid-and-invalid-blocks" id="user-content-8-objectively-valid-and-invalid-blocks"></a>

I, \{{producer\}}, acknowledge that a block is "objectively valid" if it conforms to the deterministic blockchain rules in force at the time of its creation, and is "objectively invalid" if it fails to conform to those rules.

### 9. Signing of Messages with Producer Key <a href="#user-content-9-signing-of-messages-with-producer-key" id="user-content-9-signing-of-messages-with-producer-key"></a>

I, \{{producer\}}, hereby agree to only use \{{producer\_key\}} to sign messages under the following scenarios: proposing an objectively valid block at the time appointed by the block scheduling algorithm, pre-confirming a block produced by another producer in the schedule when I find said block objectively valid, confirming a block for which \{{producer\}} has received 2/3+1 pre-confirmation messages from other producers. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a block producer for 16,000,000 blocks (approximately 90 days) on first offence or 63,000,000 blocks (approximately 365 days) on second or subsequent offenses. I may also be liable for liabilities due to this action.

### 10. Acceptance of Liability and Damages <a href="#user-content-10-acceptance-of-liability-and-damages" id="user-content-10-acceptance-of-liability-and-damages"></a>

I, \{{producer\}}, hereby accept liability for any and all provable damages that result from my: signing two different block proposals with the same timestamp with \{{producer\_key\}}, signing two different block proposals with the same block number with \{{producer\_key\}}, signing any block proposal which builds off of an objectively invalid block, signing a pre-confirmation for an objectively invalid block, signing a confirmation for a block for which I do not possess pre-confirmation messages from 2/3+1 other producers. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all Block Producers, shall be cause for my disqualification from all service as a Block Producer for 16,000,000 blocks (approximately 90 days) on first offence or 63,000,000 blocks (approximately 365 days) on second or subsequent offenses. I may also be liable for damages due to this action.

### 11. Malicious Collusion <a href="#user-content-11-malicious-collusion" id="user-content-11-malicious-collusion"></a>

I, \{{producer\}}, hereby agree that double-signing for a timestamp or block number in concert with 2 or more other Block Producers shall automatically be deemed malicious and subject to a fine equal to the past year of compensation received, and other damages. An exception may be made if \{{producer\}} can demonstrate that the double-signing occurred due to a bug in the reference software; the burden of proof is on \{{producer\}}. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a block producer for 63,000,000 blocks (approximately 365 days) on first offence or 315,000,000 blocks (approximately 5 years) on second or subsequent offenses.

### 12. Interference with Block Producer Voting Process <a href="#user-content-12-interference-with-block-producer-voting-process" id="user-content-12-interference-with-block-producer-voting-process"></a>

I, \{{producer\}}, hereby agree not to interfere with the Block Producer voting process. I agree to process all Block Producer voting transactions that occur in blocks I create, to sign all objectively valid blocks I create that contain voting transactions, and to sign all pre-confirmations and confirmations necessary to facilitate transfer of control to the next set of Block Producers as determined by the system contract. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a Block Producer for 16,000,000 blocks (approximately 90 days) on first offence or 63,000,000 blocks (approximately 365 days) on second or subsequent offenses.

### 13. Adherence to Block Producer Minimum Requirements <a href="#user-content-13-adherence-to-block-producer-minimum-requirements" id="user-content-13-adherence-to-block-producer-minimum-requirements"></a>

I, \{{producer\}}, hereby acknowledge that violation of the Libre Block Producer Minimum Requirements shall be cause for disqualification from service of \{{producer\}} as a block producer until \{{producer\}} is once again in compliance. Compliance shall be monitored and enforced by smart contract, oracle, or other objective or disinterested party as the Block Producers shall designate. The current Libre Block Producer Minimum Requirements shall be recorded on chain at a publicly disclosed address.

### 14. Authentication of Network Peers <a href="#user-content-14-authentication-of-network-peers" id="user-content-14-authentication-of-network-peers"></a>

I, \{{producer\}} may authenticate Libre Blockchain peer nodes as necessary to prevent abuse and denial of service attacks; however, \{{producer\}} agrees not to discriminate against non-abusive peers. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a Block Producer for 5,000,000 blocks (approximately 29 days) on first offence or 32,000,000 block (approximately 180 days) on second or subsequent offenses.

### 15. Fair Dealing in Processing Transactions <a href="#user-content-15-fair-dealing-in-processing-transactions" id="user-content-15-fair-dealing-in-processing-transactions"></a>

I, \{{producer\}}, agree to process transactions on a first-in, first-out, best-effort basis and to honestly bill transactions for measured execution time. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all Block Producers, shall be cause for my disqualification from all service as a Block Producer for 5,000,000 blocks (approximately 29 days) on first offence or 32,000,000 block (approximately 180 days) on second or subsequent offenses.

### 16. No Reordering Transactions to Profit <a href="#user-content-16-no-reordering-transactions-to-profit" id="user-content-16-no-reordering-transactions-to-profit"></a>

I, \{{producer\}}, agree not to manipulate the contents of blocks in order to derive profit from the order in which transactions are included in the block that I produce. Apparent intentional violation of the preceding, as judged by a 2/3+1 majority of all Block Producers, shall be cause for my disqualification from all service as a Block Producer for 16,000,000 blocks (approximately 90 days) on first offence or 63,000,000 blocks (approximately 365 days) on second or subsequent offenses.

### 17. Producing Blocks on Schedule <a href="#user-content-17-producing-blocks-on-schedule" id="user-content-17-producing-blocks-on-schedule"></a>

I, \{{producer\}}, agree not to produce blocks before my scheduled time unless I have received all blocks produced by the prior producer. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a block producer for 5,000,000 blocks (approximately 29 days) on first offence or 32,000,000 block (approximately 180 days) on second or subsequent offenses.

### 20. Producing Blocks on Accurate Time <a href="#user-content-20-producing-blocks-on-accurate-time" id="user-content-20-producing-blocks-on-accurate-time"></a>

I, \{{producer\}}, agree not to publish blocks with timestamps more than 500ms in the past or future unless the prior block is more than 75% full by either CPU or network bandwidth metrics. Apparent intentional or negligent violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a block producer for 5,000,000 blocks (approximately 29 days) on first offence or 32,000,000 block (approximately 180 days) on second or subsequent offenses.

### 21. Setting Accurate RAM Supply <a href="#user-content-21-setting-accurate-ram-supply" id="user-content-21-setting-accurate-ram-supply"></a>

I, \{{producer\}}, agree not to set the apparent RAM Supply of my block producer nodes to indicate more physical RAM than those nodes currently allocate to the Libre Blockchain. Apparent intentional violation of the preceding, as judged by a majority of 2/3+1 of all block producers, shall be cause for my disqualification from all service as a block producer for 5,000,000 blocks (approximately 29 days) on first offence or 32,000,000 block (approximately 180 days) on second or subsequent offenses.

### 22. Amending "regproducer" Human-language Contract <a href="#user-content-22-amending-regproducer-human-language-contract" id="user-content-22-amending-regproducer-human-language-contract"></a>

I, \{{producer\}}, acknowledge that the terms of this human-language contract may be amended from time to time by Libre Block Producers as described in the Libre Blockchain Operating Agreement. If I do not consent to the new terms of the amended human-language contract, I must remove mys Block Producer candidate from service. Remaining registered as a Block Producer more than 180,000 blocks (approximately 25 hours) after this human-language contract is amended indicates my acceptance of the new version.

### 23. Definitions <a href="#user-content-23-definitions" id="user-content-23-definitions"></a>

The term “Block Producer” shall be deemed to mean one of the block producer candidates listed in the“ eosio” contract‘s “schedule” table validating transactions on the Libre network, including any non-elected block producer candidate serving the validating function due to the failure of another Block Producer, or rotation schemes intended to aid network health, or any other reason. The term “second or subsequent offences” shall refer to violations of any offence described within the same paragraph of this human-language contract, but not to offences described in different paragraphs. A second or subsequence offence shall only apply if a majority of 2/3+1 block producers voted to impose a penalty on a previous alleged offence. A majority of “2/3+1” shall mean a number greater than or equal to two thirds (66.67%) of the total number plus one additional. For clarity, when the total is 21 Block Producers, a 2/3+1 majority would require the votes of 15 Block Producers. A “human-language contract” means the portion of a smart contract that is written in a human language such as English, French or Korean as opposed to a computer language such as C++, with the goal of clearly expressing the intent of the transaction between the user executing the contract and the person or entity that deployed or controls it. An "accepted third-party verification service" means a service or entity providing identification verification as a cryptographic hashed value that is listed on a list maintained on-chain by the Libre Block Producers and amended by a vote of 2/3+1 Block Producers. The "regproducer" action of the "eosio" contract means any Libre Blockchain system contract or contracts that is designated by the Block Producers to nominate an entity as a block producer candidate, whether or not the action and contract actually bears those names. The "kickbp" action of the "eosio" contract” means any Libre Blockchain system contract or contracts that is designated by the Block Producers to enforce the Libre Block Producer Minimum Requirements or "regproducer" contract, whether or not the action and contract actually bears these names. The terms "eosio’ contract" and "system contract" mean the core operating system contract or contracts of the Libre Antelope protocol, whether or not the contract is actually named “eosio”.

## Copyright: <a href="#user-content-copyright" id="user-content-copyright"></a>

This document is in the public domain.
